$(document).ready(function() {
    $("#navbarWrapper").load("/pages/navbar.html", function() {

        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems, {
            'onOpenEnd': initCarouselModal
        });

        function initCarouselModal() {
            var elems = document.querySelectorAll('.carousel');
            var instances = M.Carousel.init(elems, { 'fullWidth': true, 'indicators': true });
            instances[0].set(2);
        }

        $('.sidenav').sidenav();
        $(".dropdown-trigger").dropdown({ hover: true });
        $('.slider').slider({ indicators: false });
        $('.collapsible').collapsible();
        $('.materialboxed').materialbox();
        $('.footerWrapper').load('/pages/footer.html');
        M.updateTextFields();
    });
});